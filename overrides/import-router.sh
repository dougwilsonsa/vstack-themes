lanadaptername="$1"
wanadaptername="$2"

if [[ -z "$lanadaptername" ]]; then
  echo "Please provide the LAN adapter name as the first parameter to this script"
  exit 1
fi

if [[ -z "$wanadaptername" ]]; then
  echo "Please provide the WAN adapter name as the second parameter to this script"
  exit 1
fi

if [[ "$EUID" = 0 ]]; then
  echo "Do not run this script as sudo. It will ask you upgrade to sudo at the appropriate time"
  exit 1
fi

echo "extracting the router vdi"
cp /edgesources/openwrt-19.07.8-x86-64-combined-ext4.img.gz ~/openwrt-19.07.8-x86-64-combined-ext4.img.gz

gzip -d ~/openwrt-19.07.8-x86-64-combined-ext4.img.gz
vboxmanage convertfromraw --format VDI ~/openwrt-19.07.8-x86-64-combined-ext4.img ~/openwrt.vdi

echo "creating the router vm"
vboxmanage createvm --name WRTRouter --ostype Linux26_64 --register
UUID=$(vboxmanage list vms | grep '"WRTRouter"' | grep -o "{.*}" | sed 's/{//' | sed 's/}//')
echo "Router UUID is $UUID"

echo "creating the router vm SATA Controller"
vboxmanage storagectl $UUID --name "SATA Controller" --add sata --controller IntelAHCI --portcount 1 --bootable on

echo "attaching the router vm VDI"
vboxmanage storageattach $UUID --storagectl "SATA Controller" --device 0 --port 0 --type hdd --medium ~/openwrt.vdi

echo "modifying the router vm memory"
vboxmanage modifyvm $UUID --memory 128 

echo "setting up the router vm LAN adapter"
vboxmanage modifyvm $UUID --nic1 bridged --bridgeadapter1 $lanadaptername
echo "setting up the router vm WAN adapter"
vboxmanage modifyvm $UUID --nic2 bridged --bridgeadapter2 $wanadaptername

if [[ "$EUID" = 0 ]]; then
    echo "(1) already root"
else
    sudo -k # make sure to ask for password on next sudo
    if sudo true; then
        echo "upgraded to sudo"
    else
        echo " wrong password"
        echo "Please manually configure $lanadaptername with a static IPv4 address of 192.168.1.2, NETMASK:255.255.255.0, GW:192.168.1.1"
        exit 1
    fi
fi

echo "setting up LAN adapter with default ip of 192.168.1.2"
ifconfig $lanadaptername 192.168.1.2 netmask 255.255.255.0
route add default gw 192.168.1.1 $lanadaptername

