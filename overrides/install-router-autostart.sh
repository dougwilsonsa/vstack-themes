if [[ "$EUID" = 0 ]]; then
  echo "Do not run this script as sudo. It will ask you upgrade to sudo at the appropriate time"
  exit 1
fi

UUID=$(vboxmanage list vms | grep '"WRTRouter"' | grep -o "{.*}" | sed 's/{//' | sed 's/}//')
echo "Router UUID is $UUID"

if [[ "$EUID" = 0 ]]; then
    echo "(1) already root"
    exit 1
else
    sudo -k # make sure to ask for password on next sudo
    if sudo true; then
        echo "Upgraded to sudo"
    else
        echo "wrong password"
        exit 1
    fi
fi

echo "setting up router auto service"
cp /edgesources/autowrtrouter.service  /home/edgeadmin/autowrtrouter.service
VMUUIDREPLACEMENTSTRING="s/%%VMUUID%%/$UUID/"
sed -i $VMUUIDREPLACEMENTSTRING /home/edgeadmin/autowrtrouter.service
sudo mv /home/edgeadmin/autowrtrouter.service /usr/lib/systemd/system/autowrtrouter.service
sudo systemctl daemon-reload 
sudo systemctl enable autowrtrouter
echo "starting router autostart service"
sudo systemctl start autowrtrouter
